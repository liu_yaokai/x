cmake_minimum_required(VERSION 3.24)
project(X C)

#set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/output)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/output)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/output)

set(CMAKE_C_STANDARD 23)
if (CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
    add_compile_options("-fno-builtin")
    add_link_options("-nodefaultlibs")
    add_link_options("-nostdlib")
endif()

include_directories(xDef)

add_subdirectory(xCode)
include_directories(xCode)
link_libraries(xCode)

add_subdirectory(xParser)

#add_subdirectory(xRe-old)
add_subdirectory(xRe)

add_subdirectory(xObject)
include_directories(xObject)


add_subdirectory(xOS)

add_subdirectory(xStdlib)
include_directories(xStdlib)
link_libraries(xStdlib)

add_subdirectory(xCompiler)
add_subdirectory(xGUI)
add_subdirectory(xTest)
